function changeNumberFormat(number){
    const newNumber = Number(number.replace(/[$,]/g,''));
    if (typeof newNumber === 'number'){
        return newNumber;
    }
    else{
        return 'Enter a valid number format.'
    }
}

module.exports = changeNumberFormat;




// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 
