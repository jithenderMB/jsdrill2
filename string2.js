function validateIpAddress(element){
    const value = parseInt(element);
    if (value >= 0 &&  value <= 255) return true;
    return false;
}

function checkIpAddress(IpAddress){
    const ipArray = IpAddress.replace(/[.]/g,' ').split(' ');
    const IpArrayInt = [];
    for (let i=0;i<ipArray.length;i++){
        const check = validateIpAddress(ipArray[i]);
        if (!check){
            return 'Enter a valid IP address';
        }
        else{
            const number = parseInt(ipArray[i]);
            IpArrayInt.push(number);
        }
    }
    return IpArrayInt;
}

module.exports = checkIpAddress;


// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.
