function findMonthFromDate(dateStr){
    const months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    const date = dateStr.replace(/[/]/g,' ').split(' ');

    const condition1 = !isNaN(date[0]) && Number(date[0])>=1 && Number(date[0]) <= 12;
    const condition2 = !isNaN(date[1]) && Number(date[1]) >=1 && Number(date[1]) <= 31;
    const condition3 = !isNaN(date[1]) && Number(date[2]) >=1000 && Number(date[2])  <= 9999;

    if (condition1 && condition2 && condition3){
        const index = Number(date[0]) - 1;
        return months[index];
    }
    else{
        return 'Enter a date format like MM/DD/YYYY';
    }
}

module.exports = findMonthFromDate;


// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.
