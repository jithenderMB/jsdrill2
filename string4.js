function convertToTitleCase(name){
    let completeName = '';
    for (let parts of name){
            completeName += parts[0].toUpperCase() + parts.slice(1).toLowerCase() + ' ';
        }
    return completeName;
}

function extractNameFromObject(Object){
    const firstName = Object.first_name;
    const lastName = Object.last_name;
    let name;
    if ('middle_name' in Object){
        const middleName = Object.middle_name;
        name = [firstName,middleName,lastName];
        return convertToTitleCase(name);
    }
    else{
        name = [firstName,lastName];
        return convertToTitleCase(name);
    }
}


module.exports = extractNameFromObject;



// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
