function joinTheArrayElements(userArray){
    if (typeof userArray === 'undefined' || userArray.length === 0) return '';
    return(userArray.join(' '));
}

module.exports = joinTheArrayElements;


// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.
